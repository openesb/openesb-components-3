/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.restbc.jbiadapter.exceptions;

import java.util.List;
import javax.ws.rs.core.MediaType;


/**
 * ESBCOMP-161 Add Fault management
 * @author polperez
 * @date 17/07/2018
 * 
 * This exception is used to return the client fault (400) from the REST BC to the 
 * service consumer. The fault elements are in the InboundClientExceptionMessage.
 * We used it to associate the annotation @XmlRootElement to the message and not to the exception 
 * It is the reason why we don't have any other element in the current class.
 * 
 */


public class InboundClientException extends Exception{
    
    private InboundClientExceptionMessage mInboundErrorMessage; 
    
    public InboundClientException () {
        this.init (); 
    }
    
    /**
     * Use
     * @param message
     */
    public InboundClientException (InboundClientExceptionMessage message) {
        this.mInboundErrorMessage = message;
    }
    
    private void init() {
         mInboundErrorMessage = new InboundClientExceptionMessage();
    }

    /**
     * @return the mMessage
     */
    public InboundClientExceptionMessage getInboundErrorMessage() {
        return mInboundErrorMessage;
    }

    /**
     * @param mMessage the mMessage to set
     */
    public void setmInboundErrorMessage(InboundClientExceptionMessage mMessage) {
        this.mInboundErrorMessage = mMessage;
    }
    
    
  
}

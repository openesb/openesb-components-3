package com.sun.jbi.restbc.jbiadapter.util;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import com.sun.jbi.restbc.jbiadapter.InboundConfiguration;
import com.sun.jersey.api.uri.UriTemplate;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * PathUtil.java
 *
 * @author Edward Chou
 */
public class PathUtil {

    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final String ACCEPTED_CHARSET_KEY = "Accept-Charset";
    private static final String CONTENT_CHARSET_KEY = "charset";

    /**
     * matches a given InboundConfiguration against method, request path,
     * produce types, and consume types.
     *
     * @param inboundConfig
     * @param headers
     * @param method
     * @param path
     * @return true if match is successful, false otherwise.
     */
    public static boolean matchInboundConfiguration(
            InboundConfiguration inboundConfig,
            HttpHeaders headers,
            String method,
            String path) {

        // match method
        if (!inboundConfig.getMethod().equalsIgnoreCase(method)) {
            return false;
        }

        // match path
        UriTemplate pathTemplate = inboundConfig.getPathTemplate();
        if (!pathTemplate.match(path, new HashMap<String, String>())) {
            return false;
        }

        // match consume types
        MediaType contentType = headers.getMediaType();
        if (contentType != null) {
            boolean matchConsumeType = false;
            for (MediaType consumeType : inboundConfig.getConsumeMediaTypes()) {
                if (consumeType.isCompatible(contentType)) {
                    matchConsumeType = true;
                    break;
                }
            }
            if (!matchConsumeType) {
                return false;
            }
        }

        // match produce types
        boolean matchProduceType = false;
        List<MediaType> acceptMediaTypes = headers.getAcceptableMediaTypes();
        if (acceptMediaTypes.size() == 0) {
            matchProduceType = true;
        }
        for (MediaType acceptMediaType : acceptMediaTypes) {
            for (MediaType produceType : inboundConfig.getProduceMediaTypes()) {
                if (produceType.isCompatible(acceptMediaType)) {
                    matchProduceType = true;
                    break;
                }
            }
        }
        if (!matchProduceType) {
            return false;
        }

        return true;
    }

    /**
     * ESBCOMP-161: Add Fault Managment for REST BC Paul Perez: Pymma 12/07/2018
     *
     * Tests is the path is defined for this Inbound configuration
     *
     * @param inboundConfig
     * @param path
     * @return boolean
     */
    public static boolean matchPath(InboundConfiguration inboundConfig, String path) {
        // match path
        UriTemplate pathTemplate = inboundConfig.getPathTemplate();
        return (pathTemplate.match(path, new HashMap<String, String>()));
    }

    /**
     * ESBCOMP-161: Add Fault Managment for REST BC Paul Perez: Pymma 12/07/2018
     *
     * Tests is the method is defined for this Inbound configuration
     *
     * @param inboundConfig
     * @param path
     * @param method
     * @return boolean
     */
    public static boolean matchMethod(InboundConfiguration inboundConfig, String method) {
        // match method
        return inboundConfig.getMethod().equalsIgnoreCase(method);
    }

    /**
     * ESBCOMP-161: Add Fault Managment for REST BC Paul Perez: Pymma 12/07/2018
     * Tests if Inbound configuration consumes media type
     *
     * @param inboundConfig
     * @param headers
     * @return boolean
     */
    public static boolean matchConsumeType(InboundConfiguration inboundConfig, HttpHeaders headers) {
        /**
         * This method returns true if the content type null Else we check if
         * the content type match the consume types
         */
        MediaType contentType = headers.getMediaType();
        if (contentType != null) {
            boolean matchConsumeType = false;
            for (MediaType consumeType : inboundConfig.getConsumeMediaTypes()) {
                if (consumeType.isCompatible(contentType)) {
                    matchConsumeType = true;
                    break;
                }
            }

            return matchConsumeType;
        }
        return true;
    }

    /**
     * ESBCOMP-161: Add Fault Managment for REST BC Paul Perez: Pymma 12/07/2018
     * Tests if Inbound configuration produce media type
     *
     * @param inboundConfig
     * @param headers
     * @return boolean
     */
    public static boolean matchProduceType(InboundConfiguration inboundConfig, HttpHeaders headers) {
        // match produce types

        boolean matchProduceType = false;
        List<MediaType> acceptMediaTypes = headers.getAcceptableMediaTypes();
        if (acceptMediaTypes != null && !(acceptMediaTypes.isEmpty())) {
            for (MediaType acceptMediaType : acceptMediaTypes) {
                for (MediaType produceType : inboundConfig.getProduceMediaTypes()) {
                    if (produceType.isCompatible(acceptMediaType)) {
                        matchProduceType = true;
                        break;
                    }
                }
            }

        } else {
            matchProduceType = true;
        }
        return matchProduceType;
    }

    /**
     * ESBCOMP-161: Add Fault Management for REST BC Paul Perez: Pymma
     * 12/07/2018 Tests if accept-charset is null or contains at least one 
     * charset supported by the JVM 
     *
     * @param headers
     * @return boolean
     */
    public static boolean checkSupportedCharset(HttpHeaders headers) {
        boolean checkSupportedCharset = false;
        List<String> charsetList = headers.getRequestHeader(ACCEPTED_CHARSET_KEY);
        if (charsetList != null && !(charsetList.isEmpty())) {
            for (String charset : charsetList) {
                if (Charset.isSupported(charset)) {
                    checkSupportedCharset = true;
                    break;
                }
            }
        } else {
            checkSupportedCharset = true;
        }
        return checkSupportedCharset;
    }
    
    
    /**
     * ESBCOMP-161: Add Fault Management for REST BC Paul Perez: Pymma
     * 12/07/2018 Tests if accept-charset is null or contains one charset supported 
     * by the JVM 
     *
     * @param headers
     * @return boolean
     */
    public static boolean checkAcceptedUFT8(HttpHeaders headers) {
        boolean checkAcceptedUTF8 = false;
        List<String> charsetList = headers.getRequestHeader(ACCEPTED_CHARSET_KEY);
        if (charsetList != null && !(charsetList.isEmpty())) {
            for (String charset : charsetList) {
                if (charset.trim().equalsIgnoreCase(DEFAULT_CHARSET)) {
                    checkAcceptedUTF8 = true;
                    break;
                }
            }
        } else {
            checkAcceptedUTF8 = true;
        }
        return checkAcceptedUTF8;
    }

    /**
     * ESBCOMP-161: Add Fault Managment for REST BC Paul Perez: Pymma 12/07/2018
     * Check if the content charset is compatible with the JVM
     *
     * @param headers
     * @return boolean
     */
    public static boolean matchSupportedCharset(HttpHeaders headers) {
        /**
         * This method returns true if the charset of the content type is
         * compatible with the JVM
         */
        boolean isCompatible = false;
        MediaType contentType = headers.getMediaType();
        if (contentType == null) {
            // this will be set to UTF-8
            isCompatible = true;
        } else {
            Map<String, String> parameters = contentType.getParameters();
            String contentCharset = parameters.get(CONTENT_CHARSET_KEY);
            if (contentCharset == null) {
                // Will be Set to UTF-8
                isCompatible = true;
            } else {
                // Check of the charset of the content is compatible with the JVM
                isCompatible = Charset.isSupported(contentCharset);
            }
        }
        return isCompatible;
    }

    /**
     * ESBCOMP-161: Add Fault Managment for REST BC Paul Perez: Pymma 12/07/2018
     * Check if the content charset is UTF-8
     *
     * @param headers
     * @return boolean
     */
    public static boolean matchContentCharsetUTF8(HttpHeaders headers) {
        /**
         * This method returns true if the content type null Else we check if
         * the content type match the consume types
         */
        boolean matchContentUTF8 = false;
        MediaType contentType = headers.getMediaType();
        if (contentType == null) {
            matchContentUTF8 = true;
        } else {
            Map<String, String> parameters = contentType.getParameters();
            String contentCharset = parameters.get(CONTENT_CHARSET_KEY);
            if (contentCharset == null || contentCharset.trim().equalsIgnoreCase(DEFAULT_CHARSET)) {
                matchContentUTF8 = true;
            }
        }
        return matchContentUTF8;
    }

    /**
     * check if given MediaType belongs to a XML family of media types, such as
     * application/xml, or application/yyy+xml, or xxx/yyy+xml
     *
     * @param type
     * @return true if given MediaType belongs to XML family, false otherwise
     */
    public static boolean isXMLMediaType(MediaType type) {
        if (type.isWildcardSubtype()) {
            return true;
        }
        if (type.getSubtype().equalsIgnoreCase("xml")) {
            return true;
        }
        if (type.getSubtype().toLowerCase().endsWith("+xml")) {
            return true;
        }

        return false;
    }

    public static boolean isXMLMediaType(String type) {
        return isXMLMediaType(MediaType.valueOf(type));
    }

    /**
     * check if given MediaType belongs to a JSON family of media types, such as
     * application/json, or application/yyy+json, or xxx/yyy+json
     *
     * @param type
     * @return true if given MediaType belongs to JSON family, false otherwise
     */
    public static boolean isJSONMediaType(MediaType type) {
        if (type.isWildcardSubtype()) {
            return true;
        }
        if (type.getSubtype().equalsIgnoreCase("json")) {
            return true;
        }
        if (type.getSubtype().toLowerCase().endsWith("+json")) {
            return true;
        }

        return false;
    }

    public static boolean isJSONMediaType(String type) {
        return isJSONMediaType(MediaType.valueOf(type));
    }

    /**
     * check if given MediaType belongs to a text/plain
     *
     * @param type
     * @return true if given MediaType belongs to text/plain , false otherwise
     */
    public static boolean isTextPlainMediaType(MediaType type) {
        if (type.isWildcardSubtype()) {
            return true;
        }
        if (type.getSubtype().equalsIgnoreCase("plain") && type.getType().equalsIgnoreCase("text")) {
            return true;
        }

        return false;
    }

    public static boolean isTextPlainMediaType(String type) {
        return isTextPlainMediaType(MediaType.valueOf(type));
    }
    
    
     public static boolean isUrlEncodedMediaType(MediaType type) {
        if (type.getSubtype().equalsIgnoreCase("x-www-form-urlencoded") && type.getType().equalsIgnoreCase("application")) {
            return true;
        }

        return false;
    }

    public static boolean isUrlEncodedMediaType(String type) {
        return isUrlEncodedMediaType(MediaType.valueOf(type));
    }

}

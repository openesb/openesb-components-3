package com.sun.jbi.restbc.jbiadapter.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import static java.util.stream.Collectors.joining;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Support for application/x-www-form-urlencoded public class UrlQueryHelper {
 *
 * @author dave
 */
public class UrlQueryHelper {

    public static String QueryFromXml(String xmlstr) {
        Element element = convertSTringtoXMLNoRoot(xmlstr);
        return (NodesToQuery(element.getChildNodes()));
    }

    private static Element convertSTringtoXMLNoRoot(String xmlString) {
        Document doc = convertStringToXMLDocument(xmlString);
        return doc.getDocumentElement();
    }

    private static Document convertStringToXMLDocument(String xmlString) {
        // Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        // API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try {
            // Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();
            // Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        } catch (Exception e) {
            return null;
        }
    }

    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            return null;
        }
    }

    private static String NodesToQuery(NodeList nodeList) {
        Map<String, String> requestParams = new HashMap<>();
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node tempNode = nodeList.item(count);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                requestParams.put(tempNode.getNodeName(), tempNode.getTextContent());
            }
        }
        String requestBody = requestParams.keySet().stream()
                .map(key -> key + "=" + encodeValue(requestParams.get(key))).collect(joining("&"));
        return requestBody;
    }

    private static String decodeValue(String value) {
        try {
            return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            return null;
        }
    }

    public static String QueryToXML(String query) {
        String[] queryParts;
        String[] keyPair;
        Document doc;

        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            doc = documentBuilder.newDocument();
        } catch (Exception ex) {
            return null;
        }
        
        Element root = doc.createElement("UrlFormData");
        doc.appendChild(root);

        queryParts = query.split("&", 0);
        for (String queryPart : queryParts) {
            keyPair = queryPart.split("=");
            Element ele = doc.createElement(keyPair[0].trim());
            ele.setTextContent(decodeValue(keyPair[1].trim()));
            root.appendChild(ele);
        }
        // Transform to string

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        
        try {
            transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.transform(source, result);
            return writer.toString();
        } catch (Exception ex) {
            return null;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.restbc.jbiadapter.inbound;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

/**
 *
 * @author polperez
 */
public class TestRequestFilter implements ContainerRequestFilter{

    @Override
    public ContainerRequest filter(ContainerRequest cr) {
        String toto = "toto"; 
        System.out.println("tEST Du filter request");
        return cr;
    }
    
}

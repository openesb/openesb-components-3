package com.sun.jbi.restbc.jbiadapter;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.DeploymentException;
import javax.ws.rs.core.HttpHeaders;
import javax.xml.namespace.QName;

import com.sun.jbi.management.message.DefaultJBITaskMessageBuilder;
import com.sun.jbi.management.message.JBITaskMessageBuilder;
import com.sun.jbi.restbc.jbiadapter.exceptions.InboundClientException;
import com.sun.jbi.restbc.jbiadapter.exceptions.InboundClientExceptionMessage;
import com.sun.jbi.restbc.jbiadapter.util.PathUtil;
import com.sun.jbi.restbc.jbiadapter.wsdl.WSDLEndpoint;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

/**
 * RestSUManager.java
 *
 * @author Edward Chou
 */
public class RestSUManager implements ServiceUnitManager {

    /*
     * 31-50
     */
    private static final Logger LOGGER = Logger.getLogger(RestSUManager.class.getName());
    private static final String ACCEPTED_CHARSET_KEY = "Accept-Charset";
    private static final String CONTENT_LENGTH_KEY = "Content-Length";

    private RestComponent component;
    private ComponentContext context;

    // list of ServiceUnits loaded
    private ConcurrentMap<String, ServiceUnit> serviceUnits = new ConcurrentHashMap<String, ServiceUnit>();

    public RestSUManager(RestComponent component, ComponentContext context) {
        this.component = component;
        this.context = context;
    }

    /* (non-Javadoc)
     * @see javax.jbi.component.ServiceUnitManager#deploy(java.lang.String, java.lang.String)
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath) throws DeploymentException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            String msg = I18n.lf("RESTBC-1031: ServiceUnitManager.deploy() called serviceUnitName={0}, serviceUnitRootPath={1}", serviceUnitName, serviceUnitRootPath);//NOI18N
            LOGGER.finest(msg);
        }

        String retMsg = null;
        String taskName = "deploy";

        retMsg = createSuccessMessage(taskName, context.getComponentName());

        String[] m1 = I18n.locStr("RESTBC-5031: Deployed ServiceUnit {0}.", serviceUnitName);
        LOGGER.info(m1[2]);
        I18n.alertInfo(m1);

        return retMsg;
    }

    /* (non-Javadoc)
     * @see javax.jbi.component.ServiceUnitManager#init(java.lang.String, java.lang.String)
     */
    public void init(String serviceUnitName, String serviceUnitRootPath) throws DeploymentException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            String msg = I18n.lf("RESTBC-1032: ServiceUnitManager.init() called serviceUnitName={0}, serviceUnitRootPath={1}", serviceUnitName, serviceUnitRootPath);//NOI18N
            LOGGER.finest(msg);
        }

        String taskName = "init";

        try {
            ServiceUnit su = new ServiceUnit(component, context, serviceUnitName, serviceUnitRootPath);
            ServiceUnit prevValue = serviceUnits.putIfAbsent(su.getServiceUnitName(), su);
            if (prevValue == null) {
                if (LOGGER.isLoggable(Level.FINEST)) {
                    String msg = I18n.lf("RESTBC-1033: ServiceUnitManager.init() successful serviceUnitName={0}", serviceUnitName);//NOI18N
                    LOGGER.finest(msg);
                }
            } else {
                String msg = I18n.loc("RESTBC-7031: Duplicate ServiceUnit {0}, cannot proceed with deployment.", serviceUnitName);//NOI18N
                LOGGER.severe(msg);
                Exception ex = new Exception(msg);
                String exMsg = createExceptionMessage(context.getComponentName(),
                        taskName,
                        "FAILED",
                        "",
                        serviceUnitName,
                        ex.getLocalizedMessage(),
                        ex);
                throw new DeploymentException(exMsg, ex);
            }
        } catch (Exception ex) {
            String exMsg = createExceptionMessage(context.getComponentName(),
                    taskName,
                    "FAILED",
                    "",
                    serviceUnitName,
                    "Processing deployment error: " + ex.getLocalizedMessage(),
                    ex);
            throw new DeploymentException(exMsg, ex);
        }
    }

    /* (non-Javadoc)
     * @see javax.jbi.component.ServiceUnitManager#shutDown(java.lang.String)
     */
    public void shutDown(String serviceUnitName) throws DeploymentException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            String msg = I18n.lf("RESTBC-1034: ServiceUnitManager.shutdown() called serviceUnitName={0}", serviceUnitName);//NOI18N
            LOGGER.finest(msg);
        }

        String taskName = "shutDown";

        ServiceUnit su = serviceUnits.remove(serviceUnitName);
        if (su != null) {
            try {
                su.shutdown();
            } catch (Exception ex) {
                String msg = I18n.loc("RESTBC-7032: Error shutting down Service Unit {0} {1}", serviceUnitName, ex);//NOI18N
                LOGGER.severe(msg);
                String exMsg
                        = createExceptionMessage(context.getComponentName(),
                                taskName,
                                "FAILED",
                                "",
                                null,
                                msg,
                                ex);
                throw new DeploymentException(exMsg, ex);
            }
        } else {
            String msg = I18n.loc("RESTBC-7033: Cannot find Service Unit {0} to shutdown", serviceUnitName);//NOI18N
            LOGGER.severe(msg);
            Exception ex = new Exception(msg);
            String exMsg = createExceptionMessage(context.getComponentName(),
                    taskName,
                    "FAILED",
                    "",
                    null,
                    ex.getLocalizedMessage(),
                    ex);
            throw new DeploymentException(exMsg);
        }
    }

    /* (non-Javadoc)
     * @see javax.jbi.component.ServiceUnitManager#start(java.lang.String)
     */
    public void start(String serviceUnitName) throws DeploymentException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            String msg = I18n.lf("RESTBC-1035: ServiceUnitManager.start() called serviceUnitName={0}", serviceUnitName);//NOI18N
            LOGGER.finest(msg);
        }

        String taskName = "start";
        ServiceUnit su = serviceUnits.get(serviceUnitName);
        if (su != null) {
            try {
                su.start();
            } catch (Exception ex) {
                String msg = I18n.loc("RESTBC-7034: Error starting Service Unit {0} {1}", serviceUnitName, ex);//NOI18N
                LOGGER.severe(msg);
                String exMsg = createExceptionMessage(context.getComponentName(),
                        taskName,
                        "FAILED",
                        "",
                        null,
                        msg,
                        ex);
                throw new DeploymentException(exMsg, ex);
            }
        } else {
            String msg = I18n.loc("RESTBC-7035: Cannot find Service Unit {0} to start", serviceUnitName);//NOI18N
            LOGGER.severe(msg);
            Exception ex = new Exception(msg);
            String exMsg = createExceptionMessage(context.getComponentName(),
                    taskName,
                    "FAILED",
                    "",
                    null,
                    ex.getLocalizedMessage(),
                    ex);
            throw new DeploymentException(exMsg);
        }

    }

    /* (non-Javadoc)
     * @see javax.jbi.component.ServiceUnitManager#stop(java.lang.String)
     */
    public void stop(String serviceUnitName) throws DeploymentException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            String msg = I18n.lf("RESTBC-1036: ServiceUnitManager.stop() called serviceUnitName={0}", serviceUnitName);//NOI18N
            LOGGER.finest(msg);
        }

        String taskName = "stop";

        ServiceUnit su = serviceUnits.get(serviceUnitName);
        if (su != null) {
            try {
                su.stop();
            } catch (Exception ex) {
                String msg = I18n.loc("RESTBC-7036: Error stopping Service Unit {0} {1}", serviceUnitName, ex);//NOI18N
                LOGGER.severe(msg);
                String exMsg
                        = createExceptionMessage(context.getComponentName(),
                                taskName,
                                "FAILED",
                                "",
                                null,
                                msg,
                                ex);
                throw new DeploymentException(exMsg, ex);
            }
        } else {
            String msg = I18n.loc("RESTBC-7037: Cannot find Service Unit {0} to stop", serviceUnitName);//NOI18N
            LOGGER.severe(msg);
            Exception ex = new Exception(msg);
            String exMsg = createExceptionMessage(context.getComponentName(),
                    taskName,
                    "FAILED",
                    "",
                    null,
                    ex.getLocalizedMessage(),
                    ex);
            throw new DeploymentException(exMsg);
        }

    }

    /* (non-Javadoc)
     * @see javax.jbi.component.ServiceUnitManager#undeploy(java.lang.String, java.lang.String)
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath) throws DeploymentException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            String msg = I18n.lf("RESTBC-1037: ServiceUnitManager.undeploy() called serviceUnitName={0}, serviceUnitRootPath={1}", serviceUnitName, serviceUnitRootPath);//NOI18N
            LOGGER.finest(msg);
        }

        String taskName = "undeploy";

        return createSuccessMessage(taskName, context.getComponentName());
    }

    public OutboundConfiguration findActivatedEndpointOutbound(QName serviceName, String endpointName, QName operationName) {
        for (ServiceUnit su : serviceUnits.values()) {
            OutboundConfiguration outboundConfig = su.findActivatedEndpointOutbound(serviceName, endpointName, operationName);
            if (outboundConfig != null) {
                return outboundConfig;
            }
        }
        return null;
    }

    public InboundConfiguration findInboundConfiguration(String listenerName, HttpHeaders headers, String method, String path) {
        for (ServiceUnit su : serviceUnits.values()) {
            InboundConfiguration inboundConfig = su.findInboundConfiguration(listenerName, headers, method, path);
            if (inboundConfig != null) {
                return inboundConfig;
            }
        }
        return null;
    }

    /**
     * JIRA: ESBCOMP-161 Add Client Fault Management
     * findInboundConfigurationQuickly has been developed to improve the search
     * of the matching inbound configuration. The part of the function is
     * synchronised to avoid the concurrency conflict with the SU creation and
     * update (AFAIU). The method is also used to manage the client faults. We
     * manage in this version 404-405-408-415. An upgrade must be developed to
     * allows the user to defined their own fault management Paul Perez
     * www.pymma.com Date: 17/07/2018
     *
     * @param listenerName
     * @param headers
     * @param method
     * @param path
     * @return
     * @throws InboundClientException
     */
    public InboundConfiguration findInboundConfigurationQuickly(String listenerName, HttpHeaders headers, String method, String path) throws InboundClientException {
        ArrayList<InboundConfiguration> arrayLevel01 = new ArrayList<>();
        ArrayList<InboundConfiguration> arrayLevel02 = new ArrayList<>();
        ArrayList<InboundConfiguration> arrayLevel03 = new ArrayList<>();
        ArrayList<InboundConfiguration> arrayLevel04 = new ArrayList<>();

        /**
         * *********************************************************************
         * CHECK THE PATH --> Error code 404
         * ********************************************************************
         */
        synchronized (this) {
            for (ServiceUnit su : serviceUnits.values()) {
                List<ServiceUnitConfig> suConfigs = su.getSuConfigs();
                for (ServiceUnitConfig suConfig : suConfigs) {
                    if (suConfig instanceof PropertyServiceUnitConfig) {
                        PropertyServiceUnitConfig propertySUConfig = (PropertyServiceUnitConfig) suConfig;
                        if (!propertySUConfig.isOutbound()) {
                            InboundConfiguration inboundConfig = propertySUConfig.getInboundConfiguration();
                            if (PathUtil.matchPath(inboundConfig, path)) {
                                // Check the listerner in the same loop 
                                if (listenerName.equalsIgnoreCase(inboundConfig.getHttpListenerName())) {
                                    arrayLevel01.add(inboundConfig);
                                }
                            }
                        }
                    } else if (suConfig instanceof WSDLEndpoint) {
                        WSDLEndpoint wsdlEndpoint = (WSDLEndpoint) suConfig;
                        if (!wsdlEndpoint.isOutbound()) {
                            for (InboundConfiguration inboundConfig : wsdlEndpoint.getInboundConfiguration()) {
                                if (PathUtil.matchPath(inboundConfig, path)) {
                                    // Check the listerner in the same loop 
                                    if (listenerName.equalsIgnoreCase(inboundConfig.getHttpListenerName())) {
                                        arrayLevel01.add(inboundConfig);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /**
         * Check if the size of arrayLevel01 is equals To zero throw an 404
         * exception - We create an InboundClientExceptionMessage - fill it with
         * the error elements - Put the message in the InboundClientException
         *
         * 404 Not Found If there is no element in the array level 01 we
         * generate a 404 error
         *
         */
        if (arrayLevel01.isEmpty()) {
            InboundClientExceptionMessage faultMessage = new InboundClientExceptionMessage();
            faultMessage.setFaultCode(404);
            faultMessage.setFaultStatus(Status.fromStatusCode(404));
            faultMessage.setPath(path);
            faultMessage.setMethod(method);
            String msg = String.format("The resource referenced by the path %s does not exists", path);//NOI18N
            String technicalmsg = String.format("The resource referenced by the path %s does not exists for the listener %s", path, listenerName);//NOI18N
            faultMessage.setFaultMessage(msg);
            faultMessage.setTechnicalFaultMessage(technicalmsg);
            InboundClientException exception = new InboundClientException(faultMessage);
            // Log the fault 
            LOGGER.log(Level.SEVERE, msg);
            LOGGER.log(Level.SEVERE, technicalmsg, exception);
            throw exception;
        }

        /**
         * *********************************************************************
         * CHECK THE METHOD --> Error code 405
         * ********************************************************************
         */
        for (InboundConfiguration inboundConfig : arrayLevel01) {
            if (PathUtil.matchMethod(inboundConfig, method)) {
                arrayLevel02.add(inboundConfig);
            }
        }

        /**
         * *********************************************************************
         * 405 Method Not Allowed Check if there is the Method is supported by
         * the remaining InboundConfiguration
         * ********************************************************************
         */
        if (arrayLevel02.isEmpty()) {
            InboundClientExceptionMessage faultMessage = new InboundClientExceptionMessage();
            faultMessage.setFaultCode(405);
            faultMessage.setFaultStatus(Status.fromStatusCode(405));
            faultMessage.setPath(path);
            faultMessage.setMethod(method);
            String msg = String.format("The path %s does not implement the method %s.", path, method);
            String technicalmsg = String.format("The path %s does not implement the method %s.", path, method);
            faultMessage.setFaultMessage(msg);
            faultMessage.setTechnicalFaultMessage(technicalmsg);
            InboundClientException exception = new InboundClientException(faultMessage);
            // Log the fault 
            LOGGER.log(Level.SEVERE, msg);
            LOGGER.log(Level.SEVERE, technicalmsg, exception);
            throw exception;
        }

        /**
         * ********************************************************************
         * 417 Expectation Failed CHECK IF THE CONTENT TYPE IS DEFINED IF THE
         * CONTENT LENTH REQUEST IS NOT EMPTY.
         * *****************************************************************
         */
        List<String> requestHeader = headers.getRequestHeader(CONTENT_LENGTH_KEY);
        if (requestHeader
                != null && !(requestHeader.isEmpty())) {
            int contentLength = Integer.parseInt(requestHeader.iterator().next());
            if (contentLength > 0) {
                MediaType mediaType = headers.getMediaType();
                if (mediaType == null) {
                    InboundClientExceptionMessage faultMessage = new InboundClientExceptionMessage();
                    faultMessage.setFaultCode(417);
                    faultMessage.setFaultStatus(Status.fromStatusCode(417));
                    String msg = String.format("A request content type is expected when the body content is not empty.");
                    String technicalmsg = String.format("A request content type is expected when the body content is not empty.");
                    faultMessage.setFaultMessage(msg);
                    faultMessage.setTechnicalFaultMessage(technicalmsg);
                    InboundClientException exception = new InboundClientException(faultMessage);
                    // Log the fault 
                    LOGGER.log(Level.SEVERE, msg);
                    LOGGER.log(Level.SEVERE, technicalmsg, exception);
                    throw exception;

                }
            }
        }

        /**
         * *******************************************************************
         * CHECK IF THE REQUEST CONTENT TYPE MATCH THE RESOURCE CONSUME TYPES
         * --> Error code 415
         * *******************************************************************
         */
        for (InboundConfiguration inboundConfig : arrayLevel02) {
            if (PathUtil.matchConsumeType(inboundConfig, headers)) {
                arrayLevel03.add(inboundConfig);
            }
        }

        /**
         * 415 Unsupported Media Type The path is OK and the method OK we check
         * if the consumed type matches support the content type
         */
        if (arrayLevel03.isEmpty()) {
            InboundClientExceptionMessage faultMessage = new InboundClientExceptionMessage();
            faultMessage.setFaultCode(415);
            faultMessage.setFaultStatus(Status.fromStatusCode(415));
            faultMessage.setPath(path);
            faultMessage.setMethod(method);
            String mediaType = headers.getMediaType().toString();
            String msg = String.format("The path %s with the method %s do not support the content-type: %s.", path, method, mediaType);
            String technicalmsg = String.format("The path %s with the method %s do not support the content-type: %s.", path, method, mediaType);
            faultMessage.setFaultMessage(msg);
            faultMessage.setTechnicalFaultMessage(technicalmsg);
            InboundClientException exception = new InboundClientException(faultMessage);
            // Log the fault 
            LOGGER.log(Level.SEVERE, msg);
            LOGGER.log(Level.SEVERE, technicalmsg, exception);
            throw exception;
        }

        /**
         * *****************************************************************
         * CHECK IF THE CONTENT Charset = UTF8 --> Error code 415 May be this
         * method is not at the Good place and can be put elsewhere
         * ******************************************************************
         */
        if (!(PathUtil.matchContentCharsetUTF8(headers))) {
            MediaType contentType = headers.getMediaType();
            Map<String, String> parameters = contentType.getParameters();
            String contentCharset = parameters.get("charset");
            InboundClientExceptionMessage faultMessage = new InboundClientExceptionMessage();
            faultMessage.setFaultCode(415);
            faultMessage.setFaultStatus(Status.fromStatusCode(415));
            faultMessage.setPath(path);
            faultMessage.setMethod(method);
            String msg = String.format("The request charset defined for the content type %s. The platform support the charset UTF-8 only. Send a request with the charset UTF-8.", contentCharset);
            String technicalmsg = String.format("The request charset defined for the content type %s. The platform support the charset UTF-8 only. Send a request with the charset UTF-8.", contentCharset);
            faultMessage.setFaultMessage(msg);
            faultMessage.setTechnicalFaultMessage(technicalmsg);
            InboundClientException exception = new InboundClientException(faultMessage);
            // Log the fault 
            LOGGER.log(Level.SEVERE, msg);
            LOGGER.log(Level.SEVERE, technicalmsg, exception);
            throw exception;
        }

        /**
         * *****************************************************************
         * CHECK IF THERE IS A RESSOURCE THAT GENERATES THE ACCEPT TYPES LISTED
         * BY THE CONSUMER --> Error code 406
         * ******************************************************************
         */
        for (InboundConfiguration inboundConfig : arrayLevel03) {
            if (PathUtil.matchProduceType(inboundConfig, headers)) {
                arrayLevel04.add(inboundConfig);
            }
        }

        /**
         * 406 Not Acceptable The path is OK and the method OK, the media type
         * is OK we check if the type generated are not accepted by the consumer
         */
        if (arrayLevel04.isEmpty()) {

            List<MediaType> acceptableMediaTypes = headers.getAcceptableMediaTypes();
            InboundClientExceptionMessage faultMessage = new InboundClientExceptionMessage();
            faultMessage.setFaultCode(406);
            faultMessage.setFaultStatus(Status.fromStatusCode(406));
            faultMessage.setPath(path);
            faultMessage.setMethod(method);
            StringBuilder mediaString = new StringBuilder();
            for (MediaType type : acceptableMediaTypes) {
                mediaString.append(type.toString()).append(", ");
            }
            String msg = String.format("The path %s with the method %s could not provide the accepted content type %s ", path, method, mediaString.toString());
            String technicalmsg = String.format("The path %s with the method %s could not provide the accepted content type %s ", path, method, mediaString.toString());
            faultMessage.setFaultMessage(msg);
            faultMessage.setTechnicalFaultMessage(technicalmsg);
            InboundClientException exception = new InboundClientException(faultMessage);
            // Log the fault 
            LOGGER.log(Level.SEVERE, msg);
            LOGGER.log(Level.SEVERE, technicalmsg, exception);
            throw exception;
        }

        /**
         * *****************************************************************
         * CHECK IF THERE IS ACCEPT CHARSET CONTAINS UTF-8. The REST BC supports
         * UTF-8 only --> Error code 406
         * ******************************************************************
         */
        if (!(PathUtil.checkAcceptedUFT8(headers))) {
            InboundClientExceptionMessage faultMessage = new InboundClientExceptionMessage();
            faultMessage.setFaultCode(406);
            faultMessage.setFaultStatus(Status.fromStatusCode(406));
            faultMessage.setPath(path);
            faultMessage.setMethod(method);
            String msg = String.format("The Accepted-Charset does not contain the UTF-8 charset. The platform returns UTF-8 answer only. If possible, add UTF-8 to the accepted charset list.");
            String technicalmsg = String.format("The Accepted-Charset does not contain the UTF-8 charset. The platform returns UTF-8 answer only. If possible, add UTF-8 to the accepted charset list.");
            faultMessage.setFaultMessage(msg);
            faultMessage.setTechnicalFaultMessage(technicalmsg);
            InboundClientException exception = new InboundClientException(faultMessage);
            // Log the fault 
            LOGGER.log(Level.SEVERE, msg);
            LOGGER.log(Level.SEVERE, technicalmsg, exception);
            throw exception;
        }

        /**
         * At the end of all these test, we return the first inbound
         * configuration available
         */
        return arrayLevel04.get(
                0);

    }
    //************************ END *******************************************

    private String createSuccessMessage(String taskName, String componentName) {
        JBITaskMessageBuilder msgBuilder = new DefaultJBITaskMessageBuilder();
        msgBuilder.setComponentName(componentName);
        String retMsg = msgBuilder.createSuccessMessage(taskName);
        return retMsg;
    }

    private String createExceptionMessage(String componentName,
            String taskName,
            String status,
            String locToken,
            String locParam,
            String locMessage,
            Throwable exObj) {
        JBITaskMessageBuilder msgBuilder = new DefaultJBITaskMessageBuilder();
        msgBuilder.setComponentName(componentName);
        String retMsg = msgBuilder.createExceptionMessage(taskName, locToken, locMessage, locParam, exObj);
        return retMsg;
    }

}

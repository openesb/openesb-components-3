package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.cache.CacheManager;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.ri.EvalContext;
import org.w3c.dom.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author Paul Perez ESBCOMP-159
 */
public class PutGetValueCacheFunction extends AbstractCacheFunction {

    public PutGetValueCacheFunction(CacheManager cacheManager) {
        super(cacheManager);
    }

    public Object invoke(ExpressionContext context, Object[] parameters) {
        Node value = (Node) convertParam(parameters[1]);
        String key = (String) BPWSFunctions.convertParam(parameters[0]);        
        cacheManager.putGetValue(key, value);
        return value;
    }

    private Object convertParam(Object src) {
        if (src instanceof Pointer) {
            return ((Pointer) src).getNode();
        } else if (src instanceof EvalContext) {
            return convertParam(((EvalContext) src).getCurrentNodePointer());
        } else if (src instanceof Node) {
            //bug 615 input from doXSLTransform fuction is a Node
            return src;
        }
        return null;
    }
}

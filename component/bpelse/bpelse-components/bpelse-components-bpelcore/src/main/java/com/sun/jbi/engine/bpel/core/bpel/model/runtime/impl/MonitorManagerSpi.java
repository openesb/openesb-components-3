package com.sun.jbi.engine.bpel.core.bpel.model.runtime.impl;

import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessInstance;
import com.sun.jbi.engine.bpel.core.bpel.engine.Engine;

/**
 * @author theyoz
 * @since 26/09/18
 */
public class MonitorManagerSpi {
    public MonitorManager create(Engine engine, BPELProcessInstance instance) {
        return new MonitorManager(engine, instance);
    }
}

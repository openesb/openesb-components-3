package com.sun.jbi.engine.bpel.core.bpel.engine.impl;

import com.sun.jbi.engine.bpel.core.bpel.engine.Engine;

import javax.naming.InitialContext;
import java.util.Iterator;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * @author theyoz
 * @since 26/09/18
 */
public class EngineFactory {
    private static final Logger LOGGER = Logger.getLogger(EngineFactory.class.getName());
    private static EngineSpi factory;

    public static Engine newInstance(Properties properties, InitialContext initialContext) {
        if (factory == null) {
            ServiceLoader<EngineSpi> serviceLoader = ServiceLoader.load(EngineSpi.class);

            Iterator<EngineSpi> ite = serviceLoader.iterator();

            if (ite.hasNext()) {
                factory = ite.next();

                if (ite.hasNext()) {
                    LOGGER.warning(
                            String.format("Multiple BPELSE Engine factory found, using %s", factory.getClass().getName()));
                }
            } else {
                factory = new EngineSpi();
            }
        }

        return factory.create(properties, initialContext);
    }
}

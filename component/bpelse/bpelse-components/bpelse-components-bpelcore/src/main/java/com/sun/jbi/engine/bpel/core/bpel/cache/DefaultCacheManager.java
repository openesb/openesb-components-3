/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.engine.bpel.core.bpel.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import org.w3c.dom.Node;

/**
 *
 * @author david
 * @author polperez date : 17/01/2018 Add verification if key is null
 *
 * @polperez date:10/07/2018 Use concurrent HashMap
 */
public class DefaultCacheManager implements CacheManager {

    public static String CACHE_MANAGER_NAME = "Default Cache Manager";
    
    public DefaultCacheManager() {
    }

    @Override
    public void init(InitialContext context) throws CacheManagerException {
        // No Op
    }
    
    @Override
    public void shutdown () throws CacheManagerException{
        // No Op
    }

    /**
     * Modification ESBCOMP-158: Use synchronous BPEL Mapper Cache
     */
    private final Map<String, Node> cache = new ConcurrentHashMap<>();

    /**
     * Modification JIRA ESBCOMP-159 change method BPEL Mapper cache
     *
     * @param key
     * @return boolean
     */
    
    @Override
    public boolean contains(String key) {
        if (key == null || key.length() == 0) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINE, "Unable to check the BPEL Mapper Cache with a null key");
            throw (new RuntimeException("Unable to check the BPEL Mapper Cache with a null key"));
        }
        boolean containsKey = cache.containsKey(key);
        Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINEST, "Successfuly check the BPEL Mapper Cache with the key {0}", new Object[]{key});
        return containsKey;
    }

    @Override
    public Node get(String key) {
        if (key == null || key.length() == 0) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINE, "Unable to get a value in the BPEL Mapper Cache with a null key");
            throw (new RuntimeException("Unable to get a value in the BPEL Mapper Cache with a null key"));
        }
        Node value = cache.get(key);
        Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINEST, "Successfuly get the Value {0} with the key {1}", new Object[]{value, key});
        return value;
    }

    @Override
    public String putGetKey(String key, Node value) {
        if (key == null || key.length() == 0) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.INFO, "Unable to put a value in the BPEL Mapper Cache with a null key");
            throw (new RuntimeException("Unable to put a value in the BPEL Mapper Cache with a null key"));
        }
        if (value == null) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.INFO, "Unable to put a value in the BPEL Mapper Cache with a null value");
            throw (new RuntimeException("Unable to put a value in the BPEL Mapper Cache with a null value"));
        }
        cache.put(key, value);
        Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINEST, "Successfuly put the Value {0} with the key {1}", new Object[]{value, key});
        return key;
    }

    /**
     *
     * @param key
     * @param value
     * @return Node
     */
    @Override
    public Node putGetValue(String key, Node value) {
        if (key == null || key.length() == 0) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.INFO, "Unable to put a value in the BPEL Mapper Cache with a null key");
            throw (new RuntimeException("Unable to put a value in the BPEL Mapper Cache with a null key"));
        }
        if (value == null) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.INFO, "Unable to put a value in the BPEL Mapper Cache with a null value");
            throw (new RuntimeException("Unable to put a value in the BPEL Mapper Cache with a null value"));
        }
        cache.put(key, value);
        Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINEST, "Successfuly put the Value {0} with the key {1}", new Object[]{value, key});
        return value;
    }

    /**
     *
     * @param key
     * @param value
     * @return Node
     */
    @Override
    public Node putGetOldValue(String key, Node value) {
        if (key == null || key.length() == 0) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.INFO, "Unable to put a value in the BPEL Mapper Cache with a null key");
            throw (new RuntimeException("Unable to put a value in the BPEL Mapper Cache with a null key"));
        }
        if (value == null) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.INFO, "Unable to put a value in the BPEL Mapper Cache with a null value");
            throw (new RuntimeException("Unable to put a value in the BPEL Mapper Cache with a null value"));
        }
        Node oldValue = cache.put(key, value);
        Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINEST, "Successfuly put the Value {0} with the key {1}", new Object[]{value, key});
        return oldValue;
    }

    /**
     *
     * @param key
     * @return Node
     */
    @Override
    public Node remove(String key) {
        if (key == null || key.length() == 0) {
            Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINE, "Unable to put a value in the BPEL Mapper Cache with a null key");
            throw (new RuntimeException("Unable to remove a value in the BPEL Mapper Cache with a null key"));
        }
        Node value = cache.remove(key);
        Logger.getLogger(DefaultCacheManager.class.getName()).log(Level.FINEST, "Successfuly remove the value for the key {0}", new Object[]{key});
        return value;
    }

    @Override
    public String getCacheManagerName() {
        return CACHE_MANAGER_NAME;
    }

}

/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sun.jbi.httpsoapbc;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 30/09/18
 */
public class HttpSoapBindingLifeCycleFactory {
    private static final Logger LOGGER = Logger.getLogger(HttpSoapBindingLifeCycleFactory.class.getName());
    private static HttpSoapBindingLifeCycleSpi factory;

    public static HttpSoapBindingLifeCycle newInstance() {
        if (factory == null) {
            ServiceLoader<HttpSoapBindingLifeCycleSpi> serviceLoader = ServiceLoader.load(HttpSoapBindingLifeCycleSpi.class);

            Iterator<HttpSoapBindingLifeCycleSpi> ite = serviceLoader.iterator();

            if (ite.hasNext()) {
                factory = ite.next();

                if (ite.hasNext()) {
                    LOGGER.warning(
                            String.format("Multiple HttpSoapBindingLifeCycleSpi found, using %s", factory.getClass().getName()));
                }
            } else {
                factory = new HttpSoapBindingLifeCycleSpi();
            }
        }

        return factory.create();
    }
}


/*

 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.

 * 

 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.

 * 

 * The contents of this file are subject to the terms of either the GNU

 * General Public License Version 2 only ("GPL") or the Common

 * Development and Distribution License("CDDL") (collectively, the

 * "License"). You may not use this file except in compliance with the

 * License. You can obtain a copy of the License at

 * http://www.netbeans.org/cddl-gplv2.html

 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the

 * specific language governing permissions and limitations under the

 * License.  When distributing the software, include this License Header

 * Notice in each file and include the License file at

 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this

 * particular file as subject to the "Classpath" exception as provided

 * by Sun in the GPL Version 2 section of the License file that

 * accompanied this code. If applicable, add the following below the

 * License Header, with the fields enclosed by brackets [] replaced by

 * your own identifying information:

 * "Portions Copyrighted [year] [name of copyright owner]"

 * 

 * If you wish your version of this file to be governed by only the CDDL

 * or only the GPL Version 2, indicate your decision by adding

 * "[Contributor] elects to include this software in this distribution

 * under the [CDDL or GPL Version 2] license." If you do not indicate a

 * single choice of license, a recipient has the option to distribute

 * your version of this file under either the CDDL, the GPL Version 2 or

 * to extend the choice of license to its licensees as provided above.

 * However, if you add GPL Version 2 code and therefore, elected the GPL

 * Version 2 license, then the option applies only if the new code is

 * made subject to such option by the copyright holder.

 * 

 * Contributor(s):

 * 

 * Portions Copyrighted 2008 Sun Microsystems, Inc.

 */
package com.sun.jbi.ldapbc.util;

import com.pymma.jbi.ldapbc.util.LDAPSocketFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;
import com.sun.jbi.internationalization.Messages;
import com.sun.jbi.component.jbiext.KeyStoreUtilClient;
import com.sun.jbi.ldapbc.LdapConnectionProperties;
import com.sun.jbi.ldapbc.OperationMetaData;
import com.sun.jbi.ldapbc.PagedResultInfo;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.text.MessageFormat;
import java.util.concurrent.locks.ReentrantLock;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

/**
 *
 * @author Gary Zheng
 * @author Paul Perez @Pymma 2021 Improvement to take into account the
 * Truststore and Keystore defined in the WSDL.
 */
public class LdapConnection extends LdapConnectionProperties {

    private static final Logger mLogger = Messages.getLogger(LdapConnection.class);
    private static final Messages mMessages = Messages.getMessages(LdapConnection.class);

    private LdapContext connection;
    private String dn;
    public static String SSL_TYPE_NONE = "None";
    public static String SSL_TYPE_SSL = "Enable SSL";
    public static String SSL_TYPE_TLS = "TLS on demand";
    private final ReentrantLock lock = new ReentrantLock();

    public LdapConnection() {
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public boolean isProperty(String property) {

        String[] flds = getPropertyNames();
        for (int i = 0; i < flds.length; i++) {
            if (flds[i].equals(property)) {
                return true;
            }
        }
        return false;
    }

    public void setProperty(String property, String value) {

        try {
            Class cls = this.getClass();
            property = property.substring(0, 1).toUpperCase() + property.substring(1);
            Method method = null;
            method = cls.getMethod("set" + property, String.class);
            method.invoke(this, (Object[]) new String[]{value});

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(LdapConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Object getProperty(String property) {

        Class cls = this.getClass();
        property = property.substring(0, 1).toUpperCase() + property.substring(1);
        Method method = null;
        try {
            method = cls.getMethod("get" + property);
            return method.invoke(this, (java.lang.Object[]) null);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(LdapConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    private boolean isEmpty(String str) {
        if (null == str) {
            return true;
        }
        if (str.length() == 0) {
            return true;
        }
        return false;
    }

    private boolean isEmpty(Integer str) {
        if (null == str) {
            return true;
        }
        return false;
    }

    public void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
            PagedResultInfo.removeCookie(toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(LdapConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * The method has been completely reviewed to fix an incompatibility between
     * LDAP BC and HTTP BC. LDAP BC designer wanted to used default socket
     * factory and consequently setup JVM properties with alternative
     * Certificates. To solve this issue in this version we create a bespoke
     * socket factory. The factory is created with a SSLContext that use
     * alternative certificates as parameters
     *
     * @param meta
     * @param mKeyStoreUtil
     * @return
     * @throws Exception
     */
    @SuppressWarnings("empty-statement")
    public LdapContext getConnection(OperationMetaData meta, KeyStoreUtilClient mKeyStoreUtil) throws Exception {
        /**
         * Check if the connection already exists and there is no need for
         * recreating a connection. We recreate a connection when message
         * properties modify the connection properties. See BPEL Mapper
         */
        if (null != connection && !meta.isIsConnectionRecreate()) {
            boolean connectionAvailable = true;
            /**
             * We suppose the connection is defined if double check property is
             * required we double check the Connection
             */

            if (meta.getDoubleCheckConnection()) {
                try {
                    connection.list(meta.getSearchDN());
                } catch (Exception ex) {
                    mLogger.log(Level.WARNING, mMessages.getString("LDAPBC-W00501.LDAPBC_Double_Check_Failed", this.getLocation()));
                    // Now we must rebuild the connection. First let's close the Connection 
                    connectionAvailable = false;
                    connection.close();
                }
            }
            if (connectionAvailable) {
                return connection;
            }
        }

        // we are in the case we need to create a new connection. 
        /**
         * There are many steps to create the connection. With the element meta
         * 1- We set first the Map env for the Context 2- Since we want to use
         * alternative KeyStore and Trutstore, We use a bespoke Socket Factory
         * that will be used by the LDAP Context to take into account the
         * certificate defined in the WSDL. FYI we need a Truststore for SSL
         * handshake but also a KeyStore for a mutual identification that can be
         * required by the LDAP server. 3- we create a SSL Context to create a
         * Socket Factory that takes into account the Key and TrustStore. 4- The
         * Socket Factory creation is invoked by the LDAP Context init(env)
         *
         * Information can be found here :
         * https://docs.oracle.com/javase/8/docs/technotes/guides/jndi/jndi-ldap.html#PROP
         * https://docs.oracle.com/javase/jndi/tutorial/ldap/index.html
         * https://ldapwiki.com/wiki/Main
         */
        // Let start by seeting the Map for the LDAP Context Creation 
        Hashtable<String, String> env = new Hashtable<String, String>();

        // The context class is com.sun.jndi.ldap.LdapCtxFactory 
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

        // Context.PROVIDER_URL contains host, port and directory entry
        // The LDAP wizard does not allow multiple URL.
        if (!isEmpty(this.getLocation())) {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0215_URL", this.getLocation()));
            env.put(Context.PROVIDER_URL, this.getLocation());
        }

        // Context.SECURITY_PRINCIPAL is the login to access to the server (ex: "uid=admin,ou=sytem") 
        if (!isEmpty(this.getPrincipal())) {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0216_Principal", this.getPrincipal()));
            env.put(Context.SECURITY_PRINCIPAL, this.getPrincipal());
        }

        // Context.SECURITY_CREDENTIALS is the password associated to Context.SECURITY_PRINCIPAL
        if (!isEmpty(this.getCredential())) {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0217_Credential", mKeyStoreUtil.encrypt(this.getCredential())));
            env.put(Context.SECURITY_CREDENTIALS, this.getCredential());
        }

        /**
         * There are three options for the authentication: none, simple, strong
         * None does not send a login password Simple send a login password. It
         * must be used when the identification is required. In this version
         * strong means DIGEST-MD5. Today 2021, DIGEST-MD5 is not the best SASL
         * mechanism. Must be changed in the next version
         *
         * Context.SECURITY_AUTHENTICATION values can be none, simple or strong.
         * It cannot be "none simple" or "simple strong" . These combination can
         * be used for the connection pools:
         * System.setProperty("com.sun.jndi.ldap.connect.pool.authentication,"")
         */
        if (!isEmpty(this.getAuthentication())) {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0218_Authentication", this.getAuthentication()));
            // Change the authentication strong with DIGEST-MD5
            if (this.getAuthentication().contentEquals("strong")) {
                String replace = this.getAuthentication().replace("strong", "DIGEST-MD5");
                this.setAuthentication(replace);
            }
            env.put(Context.SECURITY_AUTHENTICATION, this.getAuthentication());
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0218A_Authentication", this.getAuthentication()));
        }

        /**
         * There are three values for the referral
         * https://docs.oracle.com/javase/jndi/tutorial/ldap/referral/jndi.html
         * follow: Automatically follow any referrals ignore: Ignore referrals
         * (default) throw: Throw a ReferralException for each referral The
         * referal can be redifined at the message level
         */
        if (!isEmpty(meta.getReferral())) {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0232_Referral", meta.getReferral()));
            env.put(Context.REFERRAL, meta.getReferral());
        }

        /**
         * Values for Context.SECURITY_PROTOCOL can be empty or "ssl"
         */
        if (!isEmpty(this.getProtocol())) {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0219_Protocol", this.getProtocol()));
            env.put(Context.SECURITY_PROTOCOL, this.getProtocol());
        }

        /**
         * Check if there is a pool required for the connection We must set the
         * env properties com.sun.jndi.ldap.connect.pool to true Then we setup
         * System Properties com.sun.jndi.ldap.connect.pool. For more detail
         * see: https://docs.oracle.com/javase/tutorial/jndi/ldap/pool.html It
         * is written when not to use the pooling!!! Pooled connections are
         * intended to be reused. Therefore, if you plan to perform operations
         * on a Context instance that might alter the underlying connection's
         * state, then you should not use connection pooling for that Context
         * instance. For example, if you plan to invoke the Start TLS extended
         * operation on a Context instance, or plan to change security-related
         * properties (such as "java.naming.security.principal" or
         * "java.naming.security.protocol") after the initial context has been
         * created, you should not use connection pooling for that Context
         * instance because the LDAP provider does not track any such state
         * changes. If you use connection pooling in such situations, you might
         * be compromising the security of your application.
         */
        if (meta.getAllowConnectionPooling()) {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0238_ALLOW_CONNPOOL", meta.getAllowConnectionPooling().toString()));
            if (!(!isEmpty(this.getSsltype()) && this.getSsltype().equals(LdapConnection.SSL_TYPE_TLS))
                    || this.getTlssecurity().toUpperCase().equals("YES")) {

                env.put("com.sun.jndi.ldap.connect.pool", "true");

                if (!isEmpty(meta.getConnectionPoolPrefSize())) {
                    mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0233_CONNPOOL_PREFSIZE", meta.getConnectionPoolPrefSize().toString()));
                    System.setProperty("com.sun.jndi.ldap.connect.pool.prefsize", meta.getConnectionPoolPrefSize().toString());
                }

                if (!isEmpty(meta.getConnectionPoolMaxSize())) {
                    mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0234_CONNPOOL_MAXSIZE", meta.getConnectionPoolMaxSize().toString()));
                    System.setProperty("com.sun.jndi.ldap.connect.pool.maxsize", meta.getConnectionPoolMaxSize().toString());
                }

                if (!isEmpty(meta.getConnectionMaxIdleTimeout())) {
                    mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0235_CONNPOOL_MAX_IDLETIME", meta.getConnectionMaxIdleTimeout().toString()));
                    System.setProperty("com.sun.jndi.ldap.connect.pool.timeout", meta.getConnectionMaxIdleTimeout().toString());
                }

                if (!isEmpty(meta.getConnectionProtocol())) {
                    mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0236_CONNPOOL_PROTOCOL", meta.getConnectionProtocol()));
                    System.setProperty("com.sun.jndi.ldap.connect.pool.protocol", meta.getConnectionProtocol());
                }

                // There are three options for the authentication: none, simple, strong
                // None does not send a login password 
                // Simple send a login password 
                // Strong means DIGEST-MD5
                if (!isEmpty(meta.getConnectionAuthentication())) {
                    mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0237_CONNPOOL_AUTHENTICATION", meta.getConnectionAuthentication()));
                    // Change the authentication strong with DIGEST-MD5
                    String connectionAuthentication = meta.getConnectionAuthentication();
                    String replaceAll = connectionAuthentication.replaceAll("strong", "DIGEST-MD5");
                    System.setProperty("com.sun.jndi.ldap.connect.pool.authentication", replaceAll);
                }
            } else {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0239_CONNPOOL_NOT_ALLOWED"));
            }
        } else {
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0238_ALLOW_CONNPOOL", meta.getAllowConnectionPooling().toString()));
        }

        /**
         * SSL and TLS require to setup an SSL context that will be used to
         * create an accurate socket factory. This bespoke socket factory does
         * not modify the System.properties and then don't impact the other
         * component that used the system keystore and trustore If keystore and
         * truststore element are not set, we use the one defined by default
         * with the JVM
         *
         */
        // define the initial context
        InitialLdapContext context = null;

        /**
         * If SSL or TLS Mode is selected we need to create an SSL context
         * Reminder that we cannot have has the same time SLL and TLS but we
         * need to have a SSLContext in both case.
         */
        SSLContext sslContext = null;
        if (this.getSsltype().equalsIgnoreCase(SSL_TYPE_SSL) || this.getSsltype().equalsIgnoreCase(SSL_TYPE_TLS)) {
            // Search for the Keystore file, Keystore password and type  
            String keystoreFileName, keystorePW, keystoreType;

            if (!isEmpty(this.getKeystore())) {
                keystoreFileName = this.getKeystore();
            } else {
                keystoreFileName = System.getProperty("javax.net.ssl.keyStore");
            }
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0223_Key_Store", keystoreFileName));

            // Search for the keystore password
            if (!isEmpty(this.getKeystorepassword())) {
                keystorePW = this.getKeystorepassword();
            } else {
                keystorePW = System.getProperty("javax.net.ssl.keyStorePassword");
            }
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0224_Key_Store_Pwd", mKeyStoreUtil.encrypt(keystorePW)));

            // Search for the keystore type 
            if (!isEmpty(this.getKeystoretype())) {
                keystoreType = this.getKeystoretype();
            } else {
                keystoreType = "JKS";
            }
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0226_Key_Store_Type", keystoreType));

            // Search for the Trustore file, Truststore password and type  
            String truststoreFileName, truststorePW, truststoreType;

            if (!isEmpty(this.getTruststore())) {
                truststoreFileName = this.getTruststore();
            } else {
                truststoreFileName = System.getProperty("javax.net.ssl.trustStore");
            }
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0220_Trust_Store", truststoreFileName));

            // Search for the truststore password
            if (!isEmpty(this.getTruststorepassword())) {
                truststorePW = this.getTruststorepassword();
            } else {
                truststorePW = System.getProperty("javax.net.ssl.trustStorePassword");
            }
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0221_Trust_Store_Pwd", mKeyStoreUtil.encrypt(truststorePW)));

            // search for the truststoretype
            if (!isEmpty(this.getTruststoretype())) {
                truststoreType = this.getTruststoretype();
            } else {
                truststoreType = "JKS";
            }

            // Create Keystore factory and KeyManager 
            mLogger.log(Level.FINE, "Start creating KeyManager");
            InputStream isKey = new FileInputStream(keystoreFileName);
            KeyStore myKeystore = KeyStore.getInstance(keystoreType);
            myKeystore.load(isKey, truststorePW.toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(myKeystore, truststorePW.toCharArray());
            KeyManager[] keyManagers = kmf.getKeyManagers();
            mLogger.log(Level.FINE, "KeyManager created");

            // Create Trustmanager Factory
            mLogger.log(Level.FINE, "Start creating TrustManager");
            InputStream isTrust = new FileInputStream(truststoreFileName);
            KeyStore myTrustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            myTrustStore.load(isTrust, truststorePW.toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(myKeystore);
            TrustManager[] trustManagers = tmf.getTrustManagers();
            mLogger.log(Level.FINE, "TrustManager created");

            // Create Secure Random 
            SecureRandom random = SecureRandom.getInstanceStrong();
            mLogger.log(Level.FINE, "Secure Random created");

            // Create the SSL context and pass the protocol as parameter in the constructor
            String protocol = this.getProtocol();
            sslContext = SSLContext.getInstance(protocol);
            sslContext.init(keyManagers, trustManagers, random);
            mLogger.log(Level.INFO, "Bespoke SSL Context Created");
        }

        /**
         * Setting up the default SSLContext requires us to use the
         * SSLContext.setDefault() method Them to invoke the new
         * InitialLdapContext(env, null). The method get connection can be
         * invoke by many threads. We don't want the SSLDefault to be modified
         * between both instructions. For that reason, We lock this piece of
         * code. There is no impact on the performance since the connection is
         * created during the SU Deployment.
         */
        /**
         * SSL is chosen. We setup the context with SSL elements
         */
        if (this.getSsltype().equalsIgnoreCase(SSL_TYPE_SSL)) {
            lock.lock();
            try {
                // Setup SocketFactory SSLContext
                LDAPSocketFactory.setSSLContext(sslContext);
                // Setup Socket factory for the environement 
                env.put("java.naming.ldap.factory.socket", LDAPSocketFactory.class.getName());
                // Retry to connect with delay between retries
                int retryInterval = meta.getRetryInterval();
                int retryCount = meta.getRetryCount();
                for (int retry = 0; retry <= retryCount; retry++) {
                    try {
                        context = new InitialLdapContext(env, null);
                        mLogger.log(Level.INFO, "LDAP InitialContext created");
                        connection = context;
                    } catch (javax.naming.CommunicationException conEx) {
                        try {
                            if (retry < retryCount) {
                                int attemptsDone = retry + 1;
                                int attemptsTodo = retryCount - retry;
                                mLogger.severe(MessageFormat.format("Failed to etablish the connection after {0} attempts. {1} attempts are remaining", attemptsDone, attemptsTodo));
                                mLogger.severe(MessageFormat.format("OpenESB will retry to connect in {0} ms", retryInterval));
                            }
                            Thread.sleep(retryInterval);
                        } catch (InterruptedException e) {
                            ;
                        }
                        if (retry == retryCount) {
                            int attemptsDone = retry + 1;
                            Logger.getLogger(LdapConnection.class.getName()).log(Level.SEVERE, "Cannot establish connection with the LDAP server after {0} attempts. Please check the external system for Connection", attemptsDone);
                            Logger.getLogger(LdapConnection.class.getName()).log(Level.SEVERE, conEx.getMessage(), conEx);
                        }
                    }
                    if (null != context) {
                        break;
                    }
                }
            } finally {
                lock.unlock();
            }
            return context;
        }

        /**
         * Case no SSL. We create the context without SSL. If TLS we negotiate a
         * TLS session after the creation of the context
         */
        //   Retry to connect with delay between retries
        int retryInterval = meta.getRetryInterval();
        int retryCount = meta.getRetryCount();
        for (int retry = 0; retry <= retryCount; retry++) {
            try {
                context = new InitialLdapContext(env, null);
                mLogger.log(Level.INFO, "LDAP InitialContext created");
                connection = context;
            } catch (javax.naming.CommunicationException conEx) {
                try {
                    if (retry < retryCount) {
                        int attemptsDone = retry + 1;
                        int attemptsTodo = retryCount - retry;
                        mLogger.severe(MessageFormat.format("Failed to etablish the connection after {0} attempts. {1} attempts are remaining", attemptsDone, attemptsTodo));
                        mLogger.severe(MessageFormat.format("OpenESB will retry to connect in {0} ms", retryInterval));
                    }
                    Thread.sleep(retryInterval);
                } catch (InterruptedException e) {
                    ;
                }
                if (retry == retryCount) {
                    int attemptsDone = retry + 1;
                    Logger.getLogger(LdapConnection.class.getName()).log(Level.SEVERE, "Cannot establish connection with the LDAP server after {0} attempts. Please check the external system for Connection", attemptsDone);
                    Logger.getLogger(LdapConnection.class.getName()).log(Level.SEVERE, conEx.getMessage(), conEx);
                }
            }
            if (null != context) {
                break;
            }
        }

        // Etablish TLS Security if needed
        if (this.getSsltype().equals(LdapConnection.SSL_TYPE_TLS)) {
            lock.lock();
            try {
                // Create a new Socket factory
                LDAPSocketFactory.setSSLContext(sslContext);
                SSLSocketFactory socketFactory = (SSLSocketFactory) LDAPSocketFactory.getDefault();
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0227_Tls_Security", this.getTlssecurity()));
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0217_Ssl_Type", this.getSsltype()));
                StartTlsResponse tls = (StartTlsResponse) context.extendedOperation(new StartTlsRequest());
                tls.negotiate(socketFactory);
                mLogger.log(Level.INFO, "LDAP TLS negociation succesful");
            } catch (Exception ex) {
                mLogger.log(Level.INFO, "Unable To etablish a LDAP TLS negociation");
            } finally {
                lock.unlock();
            }
        }
        connection = context;
        return context;
    }

    /**
     *
     *
     * @param mKeyStoreUtil
     * @return
     */
    public LdapContext getConnection(KeyStoreUtilClient mKeyStoreUtil) {

        if (null != connection) {
            return connection;
        }

        clearSystemProperties();
        LdapContext ret = null;

        try {

            if (!isEmpty(this.getTruststore())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0220_Trust_Store", this.getTruststore()));
                System.setProperty("javax.net.ssl.trustStore", this.getTruststore());
            }
            if (!isEmpty(this.getTruststoretype())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0222_Trust_Store_Type", this.getTruststoretype()));
                System.setProperty("javax.net.ssl.trustStoreType", this.getTruststoretype());
            }
            if (!isEmpty(this.getTruststorepassword())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0221_Trust_Store_Pwd", mKeyStoreUtil.encrypt(this.getTruststorepassword())));
                System.setProperty("javax.net.ssl.trustStorePassword", this.getTruststorepassword());
            }
            if (!isEmpty(this.getKeystore())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0223_Key_Store", this.getKeystore()));
                System.setProperty("javax.net.ssl.keyStore", this.getKeystore());
            }
            if (!isEmpty(this.getKeystorepassword())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0224_Key_Store_Pwd", mKeyStoreUtil.encrypt(this.getKeystorepassword())));
                System.setProperty("javax.net.ssl.keyStorePassword", this.getKeystorepassword());
            }
            if (!isEmpty(this.getKeystoreusername())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0225_Key_Store_User", this.getKeystoreusername()));
                System.setProperty("javax.net.ssl.keyStoreUsername", this.getKeystoreusername());
            }
            if (!isEmpty(this.getKeystoretype())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0226_Key_Store_Type", this.getKeystoretype()));
                System.setProperty("javax.net.ssl.keyStoreType", this.getKeystoretype());
            }

            Hashtable<String, String> env = new Hashtable<String, String>();
            if (!isEmpty(this.getPrincipal())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0216_Principal", this.getPrincipal()));
                env.put(Context.SECURITY_PRINCIPAL, this.getPrincipal());
            }

            if (!isEmpty(this.getCredential())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0217_Credential", mKeyStoreUtil.encrypt(this.getCredential())));
                env.put(Context.SECURITY_CREDENTIALS, this.getCredential());
            }

            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, this.getLocation());
            mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0215_URL", this.getLocation()));

            if (!isEmpty(this.getAuthentication())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0218_Authentication", this.getAuthentication()));
                env.put(Context.SECURITY_AUTHENTICATION, this.getAuthentication());
            }
            if (!isEmpty(this.getProtocol())) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0219_Protocol", this.getProtocol()));
                env.put(Context.SECURITY_PROTOCOL, this.getProtocol());
            }
            ret = new InitialLdapContext(env, null);

            if (this.getTlssecurity().toUpperCase().equals("YES") || this.getSsltype().equals(LdapConnection.SSL_TYPE_TLS)) {
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0227_Tls_Security", this.getTlssecurity()));
                mLogger.log(Level.INFO, mMessages.getString("LDAPBC_CFG0217_Ssl_Type", this.getSsltype()));
                StartTlsResponse tls = (StartTlsResponse) ret.extendedOperation(new StartTlsRequest());
                tls.negotiate();
            }

            ret = new InitialLdapContext(env, null);
            if (this.getTlssecurity().toUpperCase().equals("YES")) {
                StartTlsResponse tls = (StartTlsResponse) ret.extendedOperation(new StartTlsRequest());
                tls.negotiate();
            }
            connection = ret;

        } catch (Exception ex) {
            Logger.getLogger(LdapConnection.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return ret;

    }

    public void clearSystemProperties(OperationMetaData meta) {
        System.clearProperty("javax.net.ssl.trustStore");
        System.clearProperty("javax.net.ssl.trustStoreType");
        System.clearProperty("javax.net.ssl.trustStorePassword");
        System.clearProperty("javax.net.ssl.keyStore");
        System.clearProperty("javax.net.ssl.keyStorePassword");
        System.clearProperty("javax.net.ssl.keyStoreUsername");
        System.clearProperty("javax.net.ssl.keyStoreType");
        if (meta.getAllowConnectionPooling().booleanValue()) {
            System.clearProperty("com.sun.jndi.ldap.connect.pool.prefsize");
            System.clearProperty("com.sun.jndi.ldap.connect.pool.maxsize");
            System.clearProperty("com.sun.jndi.ldap.connect.pool.timeout");
            System.clearProperty("com.sun.jndi.ldap.connect.pool.protocol");
            System.clearProperty("com.sun.jndi.ldap.connect.pool.authentication");
        }
    }

    public void clearSystemProperties() {
        System.clearProperty("javax.net.ssl.trustStore");
        System.clearProperty("javax.net.ssl.trustStoreType");
        System.clearProperty("javax.net.ssl.trustStorePassword");
        System.clearProperty("javax.net.ssl.keyStore");
        System.clearProperty("javax.net.ssl.keyStorePassword");
        System.clearProperty("javax.net.ssl.keyStoreUsername");
        System.clearProperty("javax.net.ssl.keyStoreType");
    }

//    public String toString() {
//        String ret = "";
//        ret += "localtion: " + this.getLocation() +
//                "\n principal: " + this.getPrincipal() +
//                "\n credential: " + this.getCredential() +
//                "\n ssltype: " + this.getSsltype() +
//                "\n authentication: " + this.getAuthentication() +
//                "\n protocol: " + this.getProtocol() +
//                "\n truststore: " + this.getTruststore() +
//                "\n truststorepassword: " + this.getTruststorepassword() +
//                "\n truststoretype: " + this.getTruststoretype() +
//                "\n keystorepassword: " + this.getKeystorepassword() +
//                "\n keystoreusername: " + this.getKeystoreusername() +
//                "\n keystore: " + this.getKeystore() +
//                "\n keystoretype: " + this.getKeystoretype() +
//                "\n tlssecurity: " + this.getTlssecurity();
//        return ret;
//    }
}

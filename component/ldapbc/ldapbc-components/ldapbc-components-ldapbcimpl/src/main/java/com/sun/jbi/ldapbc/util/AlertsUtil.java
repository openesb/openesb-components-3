package com.sun.jbi.ldapbc.util;

/**
 *
 * @author Sun
 * @author theyoz
 */
final public class AlertsUtil {
    private static final String SERVER_TYPE_GLASSFISH = "Glassfish";
    public static final String COMPONENT_TYPE_BINDING = "BindingComponent";
    public static final String SUN_LDAP_BINDING = "sun-ldap-binding";

    public static String getServerType() {
        return SERVER_TYPE_GLASSFISH;
    }
}

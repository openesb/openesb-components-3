/**
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://opensource.org/licenses/CDDL-1.0
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://opensource.org/licenses/CDDL-1.0
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

package com.pymma.jbi.ldapbc.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 *
 * Developed by Pymma 
 * 16/12/2021
 * @author polperez
 * LDAPSocketfactory is a bespoke socket factory that uses a bespoke context factory.
 * The class design comes from the Java bug solution provided here 
 * https://bitbucket.org/atlassian/cwd-4444-java-bug-reproducer/src/master/src/main/java/Working.java
 * Here in the code, we don't use the SSLContext.getDefault() to get a SSL Context 
 * We use  the static method LDAPSocketFactory.setSSLContext().
 * In that case, we don't impact the global SSL context.
 */


public class LDAPSocketFactory extends SSLSocketFactory {
    
    SSLSocketFactory sf ; 
    private static SSLContext sslContext = null ; 
    
    public LDAPSocketFactory () throws NoSuchAlgorithmException {
        sf = sslContext.getSocketFactory();
    }
    
    @Override
    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        SSLSocket s = (SSLSocket) sf.createSocket(host,port);
        SSLParameters paramters = s.getSSLParameters();
        paramters.setEndpointIdentificationAlgorithm("LDAPS");
        s.setSSLParameters(paramters);
        return s ; 
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localhost, int localport) throws IOException, UnknownHostException {
        SSLSocket s = (SSLSocket) sf.createSocket(host,port,localhost,localport);
        SSLParameters paramters = s.getSSLParameters();
        paramters.setEndpointIdentificationAlgorithm("LDAPS");
        s.setSSLParameters(paramters);
        return s ; 
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        SSLSocket s = (SSLSocket) sf.createSocket(host,port);
        SSLParameters paramters = s.getSSLParameters();
        paramters.setEndpointIdentificationAlgorithm("LDAPS");
        s.setSSLParameters(paramters);
        return s ; 
    }

    @Override
    public Socket createSocket(InetAddress host, int port, InetAddress localhost, int localport) throws IOException {
        SSLSocket s = (SSLSocket) sf.createSocket(host,port,localhost,localport);
        SSLParameters paramters = s.getSSLParameters();
        paramters.setEndpointIdentificationAlgorithm("LDAPS");
        s.setSSLParameters(paramters);
        return s ; 
    }
    
     @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        SSLSocket sslSocket = (SSLSocket) sf.createSocket(s, host, port, autoClose);
        SSLParameters paramters = sslSocket.getSSLParameters();
        paramters.setEndpointIdentificationAlgorithm("LDAPS");
        sslSocket.setSSLParameters(paramters);
        return sslSocket ; 
    }
    
    
    public static SocketFactory getDefault () {
        try {        
            return new LDAPSocketFactory();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(LDAPSocketFactory.class.getName()).log(Level.SEVERE, null, ex);
            throw  new RuntimeException();
        }        
    }
    
    public static void  setSSLContext (SSLContext context) {
        sslContext = context ;         
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return null; 
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return null ; 
    }
    
}

/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package com.sun.jbi.ftpbc.ftp;

import com.sun.jbi.ftpbc.ftp.tests.TestUtils;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.log4testng.Logger;

import java.util.ArrayList;
import java.util.Properties;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 23/01/18
 */
public class AbstractFtpTest {
    private Logger log =  Logger.getLogger(AbstractFtpTest.class);

    protected FtpInterface ftp = null;
    protected Properties initializationProperties = null;

    @BeforeClass
    protected void before() throws Exception {
        Properties defaultProps = TestUtils.ftpServerConfiguration("PROFTPD");

        if (defaultProps == null) {
            log.warn("PROFTPD configuration was not found, test is not running");

            return;
        }

        this.initializationProperties = new Properties();
        this.initializationProperties.putAll(defaultProps);
        beforeInitializeFtp(this.initializationProperties);

        ftp = new FtpInterface();
        ftp.initialize(this.initializationProperties);

        afterInitializeFtp();
    }

    @AfterClass(alwaysRun = true)
    public void after() {
        try {
            if (ftp != null) {
                ftp.reset();
                if (ftp.getClient() != null) {
                    ftp.getClient().close();
                }
            }
        } catch (Exception e) {
            // DO NOTHING
        }

        ftp = null;
    }

    @BeforeMethod
    public void isValid() {
        if (ftp == null) {
            throw new SkipException("Ignoring test has PROFTPD configuration not found.");
        }
    }

    protected void beforeInitializeFtp(Properties props) {
        // DO NOTHING
    }

    protected void afterInitializeFtp() throws Exception {
        // DO NOTHING
    }
}

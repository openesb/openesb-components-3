/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pymma.encoder.json;

import com.sun.encoder.DataNature;
import com.sun.encoder.Encoder;
import com.sun.encoder.EncoderConfigurationException;
import com.sun.encoder.EncoderProperties;
import com.sun.encoder.EncoderProvider;
import com.sun.encoder.EncoderType;
import com.sun.encoder.MetaRef;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.xml.transform.URIResolver;

/**
 *
 * @author Aymeric
 */
public class JSONEncoderProvider implements EncoderProvider {
    
    public static final String ENCODER_ID = "jsonencoder-1.0";
    private EncoderType mEncoderType;
    
    @Override
    public String getIdentification() {
        return ENCODER_ID;
    }

    @Override
    public String[] getAliases() {
        return null;
    }

    @Override
    public Encoder newEncoder(MetaRef metaRef, URIResolver uriResolver) throws EncoderConfigurationException {
        return newEncoder(metaRef, uriResolver, new EncoderProperties());
    }

    @Override
    public Map newEncoders(Set metaRefs, URIResolver uriResolver) throws EncoderConfigurationException {
        return newEncoders(metaRefs, uriResolver, new EncoderProperties());
    }

    @Override
    public Encoder newEncoder(MetaRef metaRef, URIResolver uriResolver, EncoderProperties properties) throws EncoderConfigurationException {
        JSONEncoder encoder = new JSONEncoder(mEncoderType, properties, metaRef.getRootElemName().getLocalPart(), metaRef.getRootElemName().getNamespaceURI());
        return encoder;
    }

    @Override
    public Map newEncoders(Set metaRefs, URIResolver uriResolver, EncoderProperties properties) throws EncoderConfigurationException {
        Map res = new HashMap();
        res.put(ENCODER_ID, new JSONEncoder(mEncoderType, properties));
        return res;
    }

    @Override
    public DataNature getDataNature() {
        return DataNature.CHAR_BASED;
    }

    @Override
    public void setType(EncoderType encoderType) {
        mEncoderType = encoderType;
    }
    
}

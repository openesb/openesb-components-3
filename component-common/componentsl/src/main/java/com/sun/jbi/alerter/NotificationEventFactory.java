package com.sun.jbi.alerter;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * @author theyoz
 * @since 27/09/18
 */
public class NotificationEventFactory {
    private NotificationEventFactory() {}

    private static final Logger LOGGER = Logger.getLogger(NotificationEventFactory.class.getName());
    private static NotificationEventSpi factory;

    private static NotificationEventSpi getFactory() {
        if (factory == null) {
            ServiceLoader<NotificationEventSpi> serviceLoader = ServiceLoader.load(NotificationEventSpi.class);

            Iterator<NotificationEventSpi> ite = serviceLoader.iterator();

            if (ite.hasNext()) {
                factory = ite.next();

                if (ite.hasNext()) {
                    LOGGER.warning(
                            String.format(
                                    "Multiple NotificationEventSpi found, using %s", factory.getClass().getName()));
                }
            } else {
                factory = new NotificationEventSpi();
            }
        }

        return factory;
    }

    public static NotificationEvent newInstance() {
        return getFactory().create();
    }

    public static NotificationEvent newInstance(
            final String componentType,
            final String componentProjectPathName,
            final String componentName,
            final long timeStamp) {
        return getFactory().create(componentType, componentProjectPathName, componentName, timeStamp);
    }
}
